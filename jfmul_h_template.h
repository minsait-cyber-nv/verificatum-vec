
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#ifndef JFMUL_H_TEMPLATE_H
#define JFMUL_H_TEMPLATE_H

#include <gmp.h>
#include "vec.h"
#include "templates.h"
#include "jsmul_h_template.h"

struct FUNCTION_NAME(_vec_jfmul_tab, TAB_POSTFIX)
{

  FUNCTION_NAME(vec_jsmul_tab, TAB_POSTFIX) tab; /**< Underlying simultaneous
                                                multiplication table. */
  size_t slice_bit_len;                      /**< Bit length of each slice. */

};
typedef struct FUNCTION_NAME(_vec_jfmul_tab, TAB_POSTFIX)
FUNCTION_NAME(vec_jfmul_tab, TAB_POSTFIX); /* Magic references. */

void
FUNCTION_NAME(vec_jfmul_init, POSTFIX)
     (FUNCTION_NAME(vec_jfmul_tab, TAB_POSTFIX) *table,
      CURVE *curve,
      size_t len);

void
FUNCTION_NAME(vec_jfmul_clear_free, POSTFIX)
     (FUNCTION_NAME(vec_jfmul_tab, TAB_POSTFIX) *table);

void
FUNCTION_NAME(vec_jfmul_prcmp, POSTFIX)
     (CURVE *curve,
      FUNCTION_NAME(vec_jfmul_tab, TAB_POSTFIX) *table,
      FIELD_ELEMENT_VAR x, FIELD_ELEMENT_VAR y, FIELD_ELEMENT_VAR z);

void
FUNCTION_NAME(vec_jfmul_cmp, POSTFIX)
     (FIELD_ELEMENT_VAR ropx, FIELD_ELEMENT_VAR ropy, FIELD_ELEMENT_VAR ropz,
      CURVE *curve, FUNCTION_NAME(vec_jfmul_tab, TAB_POSTFIX) *table,
      mpz_t scalar);


#endif /* JFMUL_H_TEMPLATE_H */
