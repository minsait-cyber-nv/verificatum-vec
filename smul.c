
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include <stdio.h>

#include <gmp.h>

#include "vec.h"

void
vec_smul(mpz_t ropx, mpz_t ropy,
         vec_curve *curve,
         mpz_t *basesx, mpz_t *basesy,
         mpz_t *scalars,
         size_t len)
{
  size_t i;
  size_t bitlen;
  size_t max_scalar_bitlen;
  size_t batch_len = 100;      /* This is somewhat arbitrary, but it
                                  makes the amortized cost for squaring
                                  very small in comparison to the cost
                                  for multiplications. */
  size_t block_width;

  /* Compute the maximal bit length among the scalars. */
  max_scalar_bitlen = 0;
  for (i = 0; i < len; i++)
    {
      bitlen = mpz_sizeinbase(scalars[i], 2);
      if (bitlen > max_scalar_bitlen)
        {
          max_scalar_bitlen = bitlen;
        }
    }

  /* Determine a good block width. */
  block_width = vec_smul_block_width(max_scalar_bitlen, batch_len);

  vec_smul_block_batch(ropx, ropy,
                       curve,
                       basesx, basesy,
                       scalars,
                       len,
                       block_width, batch_len,
                       max_scalar_bitlen);
}
