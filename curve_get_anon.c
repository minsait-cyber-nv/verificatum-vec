
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include <gmp.h>

#include "vec.h"

vec_curve *
vec_curve_get(mpz_t modulus, mpz_t a, mpz_t b,
                mpz_t gx, mpz_t gy, mpz_t n)
{
  vec_curve *curve = vec_curve_alloc();

  curve->name = NULL;

  mpz_set(curve->modulus, modulus);
  mpz_set(curve->a, a);
  mpz_set(curve->b, b);

  mpz_set(curve->gx, gx);
  mpz_set(curve->gy, gy);
  mpz_set(curve->n, n);

  curve->jdbl = vec_jdbl;
  curve->jadd = vec_jadd;
  curve->jmul = vec_jmul;
}
