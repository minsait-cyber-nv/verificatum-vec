
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include <stdio.h>
#include <gmp.h>

#include "vec.h"

void
vec_mul(mpz_t rx, mpz_t ry,
        vec_curve *curve,
        mpz_t x, mpz_t y,
        mpz_t scalar)
{

  int i;
  int bit_length;

  vec_scratch_mpz_t scratch;

  vec_scratch_init_mpz_t(scratch);

  /* Initialize with the unit element. */
  mpz_set_si(rx, -1);
  mpz_set_si(ry, -1);

  /* Determine bit length. */
  bit_length = (int)mpz_sizeinbase(scalar, 2);

  for (i = bit_length; i >= 0; i--)
    {

      /* Double. */
      vec_dbl(scratch,
              rx, ry,
              curve,
              rx, ry);

      /* Add. */
      if (mpz_tstbit(scalar, i))
        {

          vec_add(scratch,
                  rx, ry,
                  curve,
                  rx, ry,
                  x, y);
        }
    }

  vec_scratch_clear_mpz_t(scratch);
}
