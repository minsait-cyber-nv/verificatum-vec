
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include <stdio.h>
#include <gmp.h>
#include "vec.h"

#include "generic_macros.h"
#include "jfmul_template.h"

size_t
vec_jfmul_ptrsize()
{
  return sizeof(vec_jfmul_tab*);
}

vec_jfmul_tab_ptr
vec_jfmul_precomp(vec_curve *curve,
                    mpz_t X, mpz_t Y, mpz_t Z,
                    size_t len)
{
  vec_jfmul_tab_ptr ptr;

  ptr.generic = (vec_jfmul_tab*)malloc(sizeof(vec_jfmul_tab));

  vec_jfmul_init(ptr.generic, curve, len);

  vec_jfmul_prcmp(curve, ptr.generic, X, Y, Z);

  return ptr;
}

void
vec_jfmul(mpz_t RX, mpz_t RY, mpz_t RZ,
            vec_curve *curve, vec_jfmul_tab_ptr table_ptr,
            mpz_t scalar)
{

  vec_jfmul_cmp(RX, RY, RZ,
                  curve, table_ptr.generic,
                  scalar);
}

void
vec_jfmul_free(vec_jfmul_tab_ptr table_ptr)
{
  vec_jfmul_clear_free(table_ptr.generic);
}
