
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include <stdio.h>
#include <gmp.h>
#include "vec.h"

void
vec_jfmul_aff(mpz_t rx, mpz_t ry,
                vec_curve *curve,
                vec_jfmul_tab_ptr table_ptr,
                mpz_t scalar)
{

  mpz_t RZ;

  mpz_init(RZ);

  curve->jfmul(rx, ry, RZ,
               curve, table_ptr,
               scalar);

  vec_jaff(rx, ry, RZ, curve);

  mpz_clear(RZ);
}
