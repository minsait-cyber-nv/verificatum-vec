
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>

#include "vec.h"

vec_curve*
vec_curve_alloc()
{
  vec_curve *curve = (vec_curve *)malloc(sizeof(vec_curve));

  mpz_init(curve->modulus);
  mpz_init(curve->a);
  mpz_init(curve->b);
  mpz_init(curve->gx);
  mpz_init(curve->gy);
  mpz_init(curve->n);

  return curve;
}
