
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include <stdio.h>
#include <gmp.h>
#include "vec.h"

void
vec_jsmul_aff(mpz_t ropx, mpz_t ropy,
              vec_curve *curve,
              mpz_t *basesx, mpz_t *basesy,
              mpz_t *exponents,
              size_t len)
{
  size_t i;

  mpz_t ropz;
  mpz_t *basesz = vec_array_alloc_init(len);

  mpz_init(ropz);

  for (i = 0; i < len; i++) {
    vec_affj(basesx[i], basesy[i], basesz[i]);
  }

  curve->jsmul(ropx, ropy, ropz,
               curve,
               basesx, basesy, basesz,
               exponents,
               len);

  vec_jaff(ropx, ropy, ropz, curve);

  mpz_clear(ropz);

  vec_array_clear_free(basesz, len);
}
