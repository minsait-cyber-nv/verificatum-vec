
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include <gmp.h>
#include "vec.h"

void
vec_jaff(mpz_t X, mpz_t Y, mpz_t Z, vec_curve *curve) {

  mpz_t ZZ;

  if (mpz_cmp_si(Z, 0) == 0)
    {
      mpz_set_si(X, -1);
      mpz_set_si(Y, -1);
    }
  else
    {

      mpz_init(ZZ);

      mpz_invert(Z, Z, curve->modulus);

      mpz_mul(ZZ, Z, Z);
      mpz_mod(ZZ, ZZ, curve->modulus);

      mpz_mul(X, X, ZZ);
      mpz_mod(X, X, curve->modulus);

      mpz_mul(Z, ZZ, Z);
      mpz_mod(Z, Z, curve->modulus);

      mpz_mul(Y, Y, Z);
      mpz_mod(Y, Y, curve->modulus);

      mpz_clear(ZZ);
    }
}
