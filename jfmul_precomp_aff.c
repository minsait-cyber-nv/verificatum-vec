
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include <stdio.h>
#include <gmp.h>
#include "vec.h"

vec_jfmul_tab_ptr
vec_jfmul_precomp_aff(vec_curve *curve,
                        mpz_t x, mpz_t y,
                        size_t len)
{
  mpz_t X;
  mpz_t Y;
  mpz_t Z;
  vec_jfmul_tab_ptr ptr;

  mpz_init(X);
  mpz_init(Y);
  mpz_init(Z);

  mpz_set(X, x);
  mpz_set(Y, y);

  vec_affj(X, Y, Z);

  ptr = curve->jfmul_precomp(curve, X, Y, Z, len);

  mpz_clear(Z);
  mpz_clear(Y);
  mpz_clear(X);

  return ptr;
}
