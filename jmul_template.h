
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

/* Naive version of multiplication. Only used during development. */

/* LCOV_EXCL_START */
#ifdef FIELD_ELEMENT

#include <stdio.h>
#include "templates.h"

void
FUNCTION_NAME(vec_jmul, POSTFIX)
     (FIELD_ELEMENT RX, FIELD_ELEMENT RY, FIELD_ELEMENT RZ,
      CURVE *curve,
      FIELD_ELEMENT_VAR X, FIELD_ELEMENT_VAR Y, FIELD_ELEMENT_VAR Z,
      mpz_t scalar)
{
  int i;
  int bitLength;

  SCRATCH(scratch);

  VEC_UNUSED(curve);

  SCRATCH_INIT(scratch);

  FIELD_ELEMENT_UNIT(RX, RY, RZ);

  /* Determine bit length. */
  bitLength = (int)mpz_sizeinbase(scalar, 2);

  for (i = bitLength; i >= 0; i--)
    {

      /* Double. */
      JDBL(scratch,
           RX, RY, RZ,
           curve,
           RX, RY, RZ);

      /* Add. */
      if (mpz_tstbit(scalar, i))
        {

          JADD(scratch,
               RX, RY, RZ,
               curve,
               RX, RY, RZ,
               X, Y, Z);
        }
    }

  SCRATCH_CLEAR(scratch);
}

#endif
/* LCOV_EXCL_STOP */
