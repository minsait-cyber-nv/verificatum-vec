
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include <stdio.h>

/*
 * Computes a "theoretical" optimal block width for a given scalar
 * length.
 */
int
vec_fmul_block_width(int bit_length, int len) {

  int width = 2;
  double cost = 1.5 * bit_length;
  double oldCost;
  double t;
  double s;
  double m;

  do {

    oldCost = cost;

    /* Amortized cost for table. */
    t = ((double)(1 << width) - width + bit_length) / len;

    /* Cost for doubling. */
    s = ((double)bit_length) / width;

    /* Cost for adding. */
    m = ((double)bit_length) / width;

    cost = t + m + s;

    width++;

  } while (width < 17 && cost < oldCost);

  width--;

  return width;
}
