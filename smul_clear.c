
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include "vec.h"

void
vec_smul_clear(vec_smul_tab table)
{
  size_t i, j;                             /* Index variables. */
  size_t tabs_len = table->tabs_len;       /* Number of sub tables. */
  size_t block_width = table->block_width; /* Width of each sub table. */
  size_t tab_len = 1 << block_width;       /* Size of each sub table. */
  mpz_t *tx;                               /* Temporary table variable. */
  mpz_t *ty;                               /* Temporary table variable. */


  for (i = 0; i < tabs_len; i++)
    {

      /* Last block may have smaller width, but the width is never
         zero. */
      if (i == tabs_len - 1)
        {
          block_width = table->len - (tabs_len - 1) * block_width;
          tab_len = 1 << block_width;
        }

      /* Deallocate all integers in table. */
      tx = table->tabsx[i];
      ty = table->tabsy[i];
      for (j = 0; j < tab_len; j++)
        {
          mpz_clear(tx[j]);
          mpz_clear(ty[j]);
        }

      /* Deallocate table. */
      free(tx);
      free(ty);
    }

  /* Deallocate table of tables. */
  free(table->tabsx);
  free(table->tabsy);
}
