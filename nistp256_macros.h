
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include "undefine_macros.h"

#define POSTFIX _nistp256_inner
#define TAB_POSTFIX _nistp256_inner

#define FIELD_ELEMENT felem
#define FIELD_ELEMENT_INIT(x)
#define FIELD_ELEMENT_CLEAR(x)
#define FIELD_ELEMENT_UNIT(x, y, z) \
  memset(x, 0, sizeof(felem));      \
  memset(y, 0, sizeof(felem));      \
  memset(z, 0, sizeof(felem))
#define FIELD_ELEMENT_SET(rx, ry, rz, x, y, z) \
  felem_assign(rx, x);                         \
  felem_assign(ry, y);                         \
  felem_assign(rz, z)

#define FIELD_ELEMENT_VAR smallfelem
#define FIELD_ELEMENT_VAR_INIT(x)
#define FIELD_ELEMENT_VAR_CLEAR(x)
#define FIELD_ELEMENT_VAR_UNIT(x, y, z) \
  memset(x, 0, sizeof(smallfelem));     \
  memset(y, 0, sizeof(smallfelem));     \
  memset(z, 0, sizeof(smallfelem))
#define FIELD_ELEMENT_VAR_SET(rx, ry, rz, x, y, z) \
  smallfelem_assign(rx, x);                        \
  smallfelem_assign(ry, y);                        \
  smallfelem_assign(rz, z)

#define FIELD_ELEMENT_CONTRACT(rx, ry, rz, x, y, z) \
  felem_contract(rx, x);                            \
  felem_contract(ry, y);                            \
  felem_contract(rz, z);


#define SCRATCH(scratch)
#define SCRATCH_INIT(scratch)
#define SCRATCH_CLEAR(scratch)

#define ARRAY_MALLOC_INIT(len) \
  (FIELD_ELEMENT_VAR *)malloc(len * sizeof(FIELD_ELEMENT_VAR))

#define ARRAY_CLEAR_FREE(array, len) free(array)

#define JDBL(scratch, rx, ry, rz, curve, x, y, z) \
  point_double(rx, ry, rz, x, y, z)

#define JDBL_VAR(scratch, rx, ry, rz, curve, x, y, z) \
  point_double_small(rx, ry, rz, x, y, z)             \

#define JADD(scratch, rx, ry, rz, curve, x1, y1, z1, x2, y2, z2) \
  point_add(rx, ry, rz, x1, y1, z1, x2, y2, z2);

#define JADD_VAR(scratch, rx, ry, rz, curve, x1, y1, z1, x2, y2, z2)  \
  point_add_small(rx, ry, rz, x1, y1, z1, x2, y2, z2)

#define CURVE vec_curve
