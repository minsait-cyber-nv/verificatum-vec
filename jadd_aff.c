
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include <stdio.h>
#include <gmp.h>
#include "vec.h"

void
vec_jadd_aff(vec_scratch_mpz_t scratch,
             mpz_t rx, mpz_t ry,
             vec_curve *curve,
             mpz_t x1, mpz_t y1,
             mpz_t x2, mpz_t y2)
{

  mpz_t X1;
  mpz_t Y1;
  mpz_t Z1;

  mpz_t X2;
  mpz_t Y2;
  mpz_t Z2;

  mpz_t Z3;

  mpz_init(X1);
  mpz_init(Y1);
  mpz_init(Z1);

  mpz_init(X2);
  mpz_init(Y2);
  mpz_init(Z2);

  mpz_init(Z3);

  mpz_set(X1, x1);
  mpz_set(Y1, y1);

  mpz_set(X2, x2);
  mpz_set(Y2, y2);

  vec_affj(X1, Y1, Z1);
  vec_affj(X2, Y2, Z2);

  curve->jadd(scratch,
              rx, ry, Z3,
              curve,
              X1, Y1, Z1,
              X2, Y2, Z2);

  vec_jaff(rx, ry, Z3, curve);

  mpz_clear(Z3);

  mpz_clear(Z2);
  mpz_clear(Y2);
  mpz_clear(X2);

  mpz_clear(Z1);
  mpz_clear(Y1);
  mpz_clear(X1);
}
