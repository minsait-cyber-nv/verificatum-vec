
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

/*
 * Computes a "theoretical" optimal block width for a given scalar
 * length.
 */
int
vec_smul_block_width(int scalars_bitlen, int batch_len) {

  /* This computes the theoretical optimum. */
  int width = 1;
  double cost = 1.5 * scalars_bitlen;
  double dbl;
  double add_precomp;
  double add;
  double old_cost;
  int width_exp;

  do {

    old_cost = cost;

    width++;
    width_exp = 1 << width;

    /* Amortized cost for doublings. */
    dbl = ((double)scalars_bitlen) / batch_len;

    /* Amortized cost for precomputing for a block. */
    add_precomp = ((double)width_exp) / width;

    /* Amortized cost for adding, given precomputation. */
    add = (((double)(scalars_bitlen)) / width) * (1 - 1.0 / width_exp);

    /* Total amortized cost. */
    cost = dbl + add_precomp + add;

  } while (cost < old_cost);

  width--;

  return width;
}
