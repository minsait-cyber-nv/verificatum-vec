
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#ifndef JSMUL_H_TEMPLATE_H
#define JSMUL_H_TEMPLATE_H

#include <gmp.h>
#include "vec.h"
#include "templates.h"

/**
 * Simultaneous multiplication table.
 */
typedef struct
{
  vec_curve *curve;           /**< Underlying curve. */
  size_t len;                 /**< Total number of bases/scalars. */
  size_t block_width;         /**< Number of bases/scalars in each block. */
  size_t tabs_len;            /**< Number of blocks. */
  FIELD_ELEMENT_VAR **tabsx;  /**< Table of tables, one sub-table for each
                                block. */
  FIELD_ELEMENT_VAR **tabsy;  /**< Table of tables, one sub-table for each
                                 block. */
  FIELD_ELEMENT_VAR **tabsz;  /**< Table of tables, one sub-table for each
                                 block. */

} FUNCTION_NAME(vec_jsmul_tab, TAB_POSTFIX)[1]; /* Magic references. */

void
FUNCTION_NAME(vec_jsmul_init, POSTFIX)
     (FUNCTION_NAME(vec_jsmul_tab, TAB_POSTFIX) table,
      CURVE *curve,
      size_t len,
      size_t block_width);

int
FUNCTION_NAME(vec_jsmul_clear, POSTFIX)
     (FUNCTION_NAME(vec_jsmul_tab, TAB_POSTFIX) table);

void
FUNCTION_NAME(vec_jsmul_precomp, POSTFIX)
     (FUNCTION_NAME(vec_jsmul_tab, TAB_POSTFIX) table,
      CURVE *curve,
      FIELD_ELEMENT_VAR *basesx, FIELD_ELEMENT_VAR *basesy,
      FIELD_ELEMENT_VAR *basesz);

void
FUNCTION_NAME(vec_jsmul_table, POSTFIX)
     (FIELD_ELEMENT_VAR ropx, FIELD_ELEMENT_VAR ropy, FIELD_ELEMENT_VAR ropz,
      CURVE *curve,
      FUNCTION_NAME(vec_jsmul_tab, TAB_POSTFIX) table,
      mpz_t *scalars,
      size_t max_scalar_bitlen);

void
FUNCTION_NAME(vec_jsmul_block_batch, POSTFIX)
     (FIELD_ELEMENT ropx, FIELD_ELEMENT ropy, FIELD_ELEMENT ropz,
      CURVE *curve,
      FIELD_ELEMENT_VAR *basesx, FIELD_ELEMENT_VAR *basesy,
      FIELD_ELEMENT_VAR *basesz,
      mpz_t *scalars,
      size_t len,
      size_t block_width, size_t batch_len,
      size_t max_scalar_bitlen);

#endif /* JSMUL_H_TEMPLATE_H */
