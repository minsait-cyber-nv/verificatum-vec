
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include <stdio.h>
#include <gmp.h>
#include "vec.h"

void
vec_smul_precomp(vec_smul_tab table, mpz_t *basesx, mpz_t *basesy)
{
  size_t i, j;        /* Index variables. */
  size_t block_width; /* Width of current subtable. */
  mpz_t *tx;          /* Temporary variable for subtable. */
  mpz_t *ty;          /* Temporary variable for subtable. */

  vec_scratch_mpz_t scratch;

  int mask;           /* Mask used for dynamic programming */
  int one_mask;       /* Mask containing a single non-zero bit. */

  vec_scratch_init_mpz_t(scratch);

  block_width = table->block_width;

  for (i = 0; i < table->tabs_len; i++)
    {
      /* Last block may have smaller width, but the width is never
         zero. */
      if (i == table->tabs_len - 1)
        {
          block_width = table->len - (table->tabs_len - 1) * block_width;
        }

      /* Current subtable. */
      tx = table->tabsx[i];
      ty = table->tabsy[i];

      /* Initialize current subtable with all trivial products. */
      mpz_set_si(tx[0], -1);
      mpz_set_si(ty[0], -1);

      mask = 1;
      for (j = 0; j < block_width; j++)
        {
          mpz_set(tx[mask], basesx[j]);
          mpz_set(ty[mask], basesy[j]);
          mask <<= 1;
        }

      /* Initialize current subtable with all non-trivial products. */
      for (mask = 1; mask < (1 << block_width); mask++)
        {
          one_mask = mask & (-mask);

          vec_add(scratch,
                  tx[mask], ty[mask],
                  table->curve,
                  tx[mask ^ one_mask], ty[mask ^ one_mask],
                  tx[one_mask], ty[one_mask]);
        }

      basesx += block_width;
      basesy += block_width;
    }

  vec_scratch_clear_mpz_t(scratch);
}
