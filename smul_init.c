
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include "vec.h"

void
vec_smul_init(vec_smul_tab table,
              vec_curve *curve,
              size_t len, size_t block_width)
{
  size_t i, j;     /* Index parameters. */
  size_t tab_len;  /* Size of a subtable. */
  mpz_t *tx;       /* Temporary variable for subtable. */
  mpz_t *ty;       /* Temporary variable for subtable. */

  /* Initialize the curve parameters of the table. */
  table->curve = curve;

  /* Initialize length parameters. */
  table->len = len;
  table->block_width = block_width;
  if (len < block_width) {
    table->block_width = len;
  }
  table->tabs_len = (len + block_width - 1) / block_width;

  /* Allocate and initialize space for pointers to tables. */
  table->tabsx = (mpz_t **)malloc(table->tabs_len * sizeof(mpz_t *));
  table->tabsy = (mpz_t **)malloc(table->tabs_len * sizeof(mpz_t *));

  tab_len = 1 << block_width;
  for (i = 0; i < table->tabs_len; i++)
    {

      /* Last block may be more narrow than the other, but it is never
         of size zero. */
      if (i == table->tabs_len - 1
          && len - (table->tabs_len - 1) * block_width < block_width)
        {
          block_width = len - (table->tabs_len - 1) * block_width;
          tab_len = 1 << block_width;
        }

      /* Allocate and initialize a table. */
      table->tabsx[i] = (mpz_t *)malloc(tab_len * sizeof(mpz_t));
      tx = table->tabsx[i];
      table->tabsy[i] = (mpz_t *)malloc(tab_len * sizeof(mpz_t));
      ty = table->tabsy[i];

      /* Initialize mpz_t's. */
      for (j = 0; j < tab_len; j++)
        {
          mpz_init(tx[j]);
          mpz_init(ty[j]);
        }
    }
}
