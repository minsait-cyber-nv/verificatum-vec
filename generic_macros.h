
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include "undefine_macros.h"

#define SCRATCH(scratch) vec_scratch_mpz_t scratch

#define POSTFIX _generic_inner
#define TAB_POSTFIX _generic_inner

#define FIELD_ELEMENT mpz_t
#define FIELD_ELEMENT_INIT(x) mpz_init(x)
#define FIELD_ELEMENT_CLEAR(x) mpz_clear(x)
#define FIELD_ELEMENT_UNIT(x, y, z) \
  mpz_set_si(x, 0);        \
  mpz_set_si(y, 1);        \
  mpz_set_si(z, 0)
#define FIELD_ELEMENT_SET(rx, ry, rz, x, y, z)    \
  mpz_set(rx, x);                                 \
  mpz_set(ry, y);                                 \
  mpz_set(rz, z)

#define FIELD_ELEMENT_VAR mpz_t
#define FIELD_ELEMENT_VAR_INIT(x) mpz_init(x)
#define FIELD_ELEMENT_VAR_CLEAR(x) mpz_clear(x)
#define FIELD_ELEMENT_VAR_UNIT(x, y, z) \
  mpz_set_si(x, 0);                     \
  mpz_set_si(y, 1);                     \
  mpz_set_si(z, 0)
#define FIELD_ELEMENT_VAR_SET(rx, ry, rz, x, y, z) \
  mpz_set(rx, x);                                  \
  mpz_set(ry, y);                                  \
  mpz_set(rz, z)

#define FIELD_ELEMENT_CONTRACT(rx, ry, rz, x, y, z) \
  mpz_set(rx, x);                                   \
  mpz_set(ry, y);                                   \
  mpz_set(rz, z)                                    \

#define SCRATCH_INIT(scratch) vec_scratch_init_mpz_t(scratch)
#define SCRATCH_CLEAR(scratch) vec_scratch_clear_mpz_t(scratch)

#define ARRAY_MALLOC_INIT(len) vec_array_alloc_init(len)

#define ARRAY_CLEAR_FREE(array, len) vec_array_clear_free(array, len)

#define JDBL(scratch, rx, ry, rz, curve, x, y, z) \
  curve->jdbl(scratch,                            \
              rx, ry, rz,                         \
              curve,                              \
              x, y, z)

#define JDBL_VAR(scratch, rx, ry, rz, curve, x, y, z)  \
  curve->jdbl(scratch,                                 \
              rx, ry, rz,                              \
              curve,                                   \
              x, y, z)

#define JADD(scratch, rx, ry, rz, curve, x1, y1, z1, x2, y2, z2) \
  curve->jadd(scratch,                                           \
              rx, ry, rz,                                        \
              curve,                                             \
              x1, y1, z1,                                        \
              x2, y2, z2)

#define JADD_VAR(scratch, rx, ry, rz, curve, x1, y1, z1, x2, y2, z2)  \
  curve->jadd(scratch,                                                \
              rx, ry, rz,                                             \
              curve,                                                  \
              x1, y1, z1,                                             \
              x2, y2, z2)

#define CURVE vec_curve
